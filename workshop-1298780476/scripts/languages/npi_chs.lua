-- Chinese Simplified
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "平息阶段",
        warn = "警告阶段",
        wild = "暴动阶段",
        dawn = "过渡阶段",
        lock = "暴动阶段锁定",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}将以 {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
