-- Korean
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "차분한 위상",
        warn = "단계 경고",
        wild = "단계 야생",
        dawn = "상상의 새벽",
        lock = "단계 야생 잠긴",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME} 끝날거야 {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
