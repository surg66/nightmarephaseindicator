-- Czech
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fáze klid",
        warn = "Fáze varuje",
        wild = "Fáze divoké",
        dawn = "Fáze svítání",
        lock = "Fáze divoké zamčené",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, skončí v {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
