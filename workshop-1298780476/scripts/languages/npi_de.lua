-- German
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Phase ruhig",
        warn = "Phase warnen",
        wild = "Phase wild",
        dawn = "Phase abschluss",
        lock = "Phase wild gesperrt",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, wird in enden {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
