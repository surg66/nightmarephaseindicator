-- Portuguese
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Fase Calma",
        warn = "Fase de Aviso",
        wild = "Fase Selvagem",
        dawn = "Fase de Alvorecer",
        lock = "Fase Selvagem Bloqueada",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, vai acabar em {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
