-- Ukrainian
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Фаза спокою",
        warn = "Фаза тривоги",
        wild = "Дика фаза",
        dawn = "Фаза свiтанку", 
        lock = "Дика фаза закрiплена",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, закінчиться через {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
