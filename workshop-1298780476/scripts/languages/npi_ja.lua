-- Japanese
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "穏やかな位相",
        warn = "フェーズ警告",
        wild = "フェーズワイルド",
        dawn = "相明の夜明け",
        lock = "フェーズワイルドロックされた",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}で終わります {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
