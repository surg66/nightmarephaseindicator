-- Polish
GLOBAL.STRINGS.NIGHMARE_PHASE_INDICATOR =
{
    PHASENAMES = 
    {
        calm = "Faza spokojna",
        warn = "Faza ostrzegawcza",
        wild = "Faza dzika",
        dawn = "Faza wieczorna",
        lock = "Faza dzika zamkięta",
    },

    ANNOUNCE =
    {
        FORMAT_STRING = "{PHASENAME}, skończy się za {TIMELEFT}",
        NOTIME_FORMAT_STRING = "{PHASENAME}",
    }
}
